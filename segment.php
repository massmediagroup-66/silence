<?php
declare(strict_types=1);

use App\Extractor\SegmentExtractor;
use App\Extractor\SilenceExtractor;
use App\Model\Segments;
use JMS\Serializer\SerializerBuilder;

require_once './vendor/autoload.php';

list($xmlData, $minIntervalBetweenChapters, $maxChapterDuration, $minIntervalBetweenParts) = getParameters($argv);

$silences = (new SilenceExtractor($xmlData))->extract();

$segmentExtractor = new SegmentExtractor(
    $silences,
    $minIntervalBetweenChapters,
    $maxChapterDuration,
    $minIntervalBetweenParts
);

$segments = $segmentExtractor->extract();

printSegments($segments);

function getParameters(array $argv)
{
    if (count($argv) < 5) {
        echo("Invalid parameters count\n");
        die();
    }

    $errors = '';

    $xmlData = file_get_contents($argv[1]);
    if (!$xmlData) {
        $errors .= "Have no xml data \n";
    }

    try {
        $minIntervalBetweenChapters = new DateInterval($argv[2]);
    } catch (Exception $e) {
        $errors .= "Invalid parameter minIntervalBetweenChapters\n";
    }

    try {
        $maxChapterDuration = new DateInterval($argv[3]);
    } catch (Exception $e) {
        $errors .= "Invalid parameter minIntervalBetweenChapters\n";
    }

    try {
        $minIntervalBetweenParts = new DateInterval($argv[4]);
    } catch (Exception $e) {
        $errors .= "Invalid parameter minIntervalBetweenChapters\n";
    }

    if ($errors) {
        echo($errors);
        die();
    }

    return array($xmlData, $minIntervalBetweenChapters, $maxChapterDuration, $minIntervalBetweenParts);
}

function printSegments(Segments $segments)
{
    $serializer = SerializerBuilder::create()->build();
    $json = $serializer->serialize($segments, 'json');

    echo $json;
    echo "\n";
}
