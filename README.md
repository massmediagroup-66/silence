# Assignment: divide an audio stream by silence
[details](./condition/silence.md)

### Setup
```
composer install
```

### Console command
```
php segment.php
```  

### Command example
```
php segment.php ./condition/silence-files/silence1.xml PT5S PT60S PT3S
```  
