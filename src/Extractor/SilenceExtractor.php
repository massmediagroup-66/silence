<?php
declare(strict_types=1);

namespace App\Extractor;

use App\Model\Silence;
use App\Model\Silences;
use JMS\Serializer\SerializerBuilder;

class SilenceExtractor
{
    private string $xmlData;

    public function __construct(string $xmlData)
    {
        $this->xmlData = $xmlData;
    }

    /**
     * @return Silence[]
     */
    public function extract(): array
    {
        $serializer = SerializerBuilder::create()->build();
        $silences = $serializer->deserialize($this->xmlData, Silences::class, 'xml');

        return $silences->getSilences();
    }
}
