<?php
declare(strict_types=1);

namespace App\Extractor;

use App\Model\Chapter;
use App\Model\Segment;
use App\Model\Segments;
use App\Model\Silence;
use DateInterval;
use DateTimeImmutable;

class SegmentExtractor
{
    /** @var Silence[] */
    private array $silences;
    private DateInterval $minIntervalBetweenChapters;
    private DateInterval $maxChapterDuration;
    private DateInterval $minIntervalBetweenParts;

    public function __construct(
        array $silences,
        DateInterval $minIntervalBetweenChapters,
        DateInterval $maxChapterDuration,
        DateInterval $minIntervalBetweenParts
    ) {
        $this->silences = $silences;
        $this->minIntervalBetweenChapters = $minIntervalBetweenChapters;
        $this->maxChapterDuration = $maxChapterDuration;
        $this->minIntervalBetweenParts = $minIntervalBetweenParts;
    }

    public function extract(): Segments
    {
        $chapters = $this->makeChapters();

        $chapters = $this->makeParts($chapters);

        return $this->makeSegments($chapters);
    }

    protected function createChapter(int $currentChapterIndex, DateInterval $currentOffset): Chapter
    {
        $segment = new Segment();
        $title = sprintf('%s %d', Segment::CHAPTER_TITLE_PREFIX, $currentChapterIndex);

        $segment
            ->setTitle($title)
            ->setOffset($currentOffset)
        ;

        return (new Chapter())->setSegment($segment);
    }

    protected function createPart(DateInterval $currentOffset): Segment
    {
        $segment = new Segment();

        $segment
            ->setTitle('')
            ->setOffset($currentOffset)
        ;

        return $segment;
    }

    protected function getIntervalDiff(DateInterval $a, DateInterval $b): DateInterval
    {
        $base = new DateTimeImmutable;

        return $base->add($b)->diff($base->add($a));
    }

    protected function compareInterval(DateInterval $a, DateInterval $b): int
    {
        $base = new DateTimeImmutable;
        $aTime = $base->add($a);
        $bTime = $base->add($b);

        if ($aTime > $bTime) {
            return 1;
        } else if ($aTime < $bTime) {
            return -1;
        } else {
            return 0;
        }
    }

    /**
     * @return Chapter[]
     */
    protected function makeChapters(): array
    {
        $currentChapterIndex = 1;
        $chapterOffset = new DateInterval('PT0S');

        $chapter = $this->createChapter($currentChapterIndex, $chapterOffset);
        $chapter->addPart($this->createPart($chapterOffset));
        $chapters = [$chapter];

        foreach ($this->silences as $silence) {
            $silenceDuration = $this->getIntervalDiff($silence->getUntil(), $silence->getFrom());

            if (
                $this->compareInterval(
                    $silenceDuration,
                    $this->minIntervalBetweenParts
                ) >= 0
                && $this->compareInterval(
                    $silenceDuration,
                    $this->minIntervalBetweenChapters
                ) < 0
            ) {
                $chapter->addPart($this->createPart($silence->getUntil()));
            }

            if (
                $this->compareInterval(
                    $silenceDuration,
                    $this->minIntervalBetweenChapters
                ) >= 0
            ) {
                $currentChapterIndex++;
                $chapterOffset = $silence->getUntil();
                $chapter = $this->createChapter($currentChapterIndex, $chapterOffset);
                $chapters[] = $chapter;
            }
        }
        return $chapters;
    }

    /**
     * @param Chapter[] $chapters
     * @return Chapter[]
     */
    protected function makeParts(array $chapters): array
    {
        $chapterOffset = new DateInterval('PT0S');
        for ($chapterIndex = 0; $chapterIndex < count($chapters); $chapterIndex++) {
            /** @var Segment $segment */
            $chapter = $chapters[$chapterIndex];

            if (isset($chapters[$chapterIndex + 1])) {
                $segmentToCalculateDuration = $chapters[$chapterIndex + 1]->getSegment();
            } else {
                $segmentToCalculateDuration = $chapter->getLastPart();
            }

            if (!$segmentToCalculateDuration) {
                continue;
            }

            $chapterDuration = $this->getIntervalDiff($chapterOffset, $segmentToCalculateDuration->getOffset());
            if ($this->compareInterval(
                    $chapterDuration,
                    $this->maxChapterDuration
                ) >= 0
            ) {
                $chapter->setParts([]);
            } else {
                for ($partIndex = 0; $partIndex < count($chapter->getParts()); $partIndex++) {
                    $part = $chapter->getPart($partIndex);

                    $title = sprintf(
                        '%s %d %s %d',
                        Segment::CHAPTER_TITLE_PREFIX,
                        $chapterIndex + 1,
                        Segment::PART_TITLE_PREFIX,
                        $partIndex + 1
                    );

                    $part->setTitle($title);
                }
            }

            $chapterOffset = $chapterDuration;
        }

        return $chapters;
    }

    /**
     * @param Chapter[] $chapters
     * @return Segments
     */
    protected function makeSegments(array $chapters): Segments
    {
        $segments = new Segments();
        for ($i = 0; $i < count($chapters); $i++) {
            $chapter = $chapters[$i];
            $segment = $chapter->getSegment();

            if (count($chapter->getParts()) < 2) {
                $segments->addSegment($segment);
            } else {
                foreach ($chapter->getParts() as $part) {
                    $segments->addSegment($part);
                }
            }
        }

        return $segments;
    }
}
