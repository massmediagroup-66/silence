<?php
declare(strict_types=1);

namespace App\Model;

use DateInterval;
use JMS\Serializer\Annotation as Serializer;

class Segment
{
    public const CHAPTER_TITLE_PREFIX = 'Chapter';
    public const PART_TITLE_PREFIX = 'Part';

    /**
     * @Serializer\Type("string")
     * @Serializer\SerializedName("title")
     */
    private string $title;

    /**
     * @Serializer\Type("string")
     * @Serializer\Accessor(getter="getOffsetIso")
     * @Serializer\SerializedName("offset")
     */
    private DateInterval $offset;

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title): Segment
    {
        $this->title = $title;

        return $this;
    }

    public function getOffset(): DateInterval
    {
        return $this->offset;
    }

    public function getOffsetIso(): string
    {
        return $this->offset->format('PT%hH%IM%S.%FS');
    }

    public function setOffset(DateInterval $offset): Segment
    {
        $this->offset = $offset;

        return $this;
    }
}
