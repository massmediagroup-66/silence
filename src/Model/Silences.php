<?php
declare(strict_types=1);

namespace App\Model;

use JMS\Serializer\Annotation as Serializer;

class Silences
{
    /**
     * @var Silence[]
     *
     * @Serializer\Type("array<App\Model\Silence>")
     * @Serializer\XmlList(inline=true, entry="silence")
     */
    private array $silences;

    /**
     * @return Silence[]
     */
    public function getSilences(): array
    {
        return $this->silences;
    }

    /**
     * @param Silence[] $silences
     */
    public function setSilences(array $silences): Silences
    {
        $this->silences = $silences;

        return $this;
    }
}
