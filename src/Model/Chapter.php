<?php
declare(strict_types=1);

namespace App\Model;

class Chapter
{
    private Segment $segment;

    /** @var Segment[] */
    private array $parts = [];

    public function getSegment(): Segment
    {
        return $this->segment;
    }

    public function setSegment(Segment $segment): Chapter
    {
        $this->segment = $segment;

        return $this;
    }

    /**
     * @return Segment[]
     */
    public function getParts(): array
    {
        return $this->parts;
    }

    /**
     * @param int $index
     * @return Segment|null
     */
    public function getPart(int $index): ?Segment
    {
        return $this->parts[$index] ?? null;
    }

    /**
     * @return Segment|null
     */
    public function getLastPart(): ?Segment
    {
        $last = array_key_last($this->parts);

        return $last ? $this->parts[$last] : null;
    }

    /**
     * @param Segment[] $parts
     * @return Chapter
     */
    public function setParts(array $parts): Chapter
    {
        $this->parts = $parts;

        return $this;
    }

    /**
     * @param Segment $part
     * @return Chapter
     */
    public function addPart(Segment $part): Chapter
    {
        $this->parts[] = $part;

        return $this;
    }
}
