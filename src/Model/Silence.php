<?php
declare(strict_types=1);

namespace App\Model;

use DateInterval;
use JMS\Serializer\Annotation as Serializer;

class Silence
{
    /**
     * @Serializer\Type("DateInterval")
     * @Serializer\XmlAttribute()
     */
    private DateInterval $from;

    /**
     * @Serializer\Type("DateInterval")
     * @Serializer\XmlAttribute()
     */
    private DateInterval $until;

    public function getFrom(): DateInterval
    {
        return $this->from;
    }

    public function setFrom(DateInterval $from): Silence
    {
        $this->from = $from;

        return $this;
    }

    public function getUntil(): DateInterval
    {
        return $this->until;
    }

    public function setUntil(DateInterval $until): Silence
    {
        $this->until = $until;

        return $this;
    }
}
