<?php
declare(strict_types=1);

namespace App\Model;

use JMS\Serializer\Annotation as Serializer;

class Segments
{
    /**
     * @var Segment[]
     *
     * @Serializer\SerializedName("segments")
     * @Serializer\Type("array<App\Model\Segment>")
     */
    private array $segments = [];

    /**
     * @return Segment[]
     */
    public function getSegments(): array
    {
        return $this->segments;
    }

    public function addSegment(Segment $segment): Segments
    {
        $this->segments[] = $segment;

        return $this;
    }
}
